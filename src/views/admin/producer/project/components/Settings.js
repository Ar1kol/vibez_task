/*!
  _   _  ___  ____  ___ ________  _   _   _   _ ___   ____  ____   ___  
 | | | |/ _ \|  _ \|_ _|__  / _ \| \ | | | | | |_ _| |  _ \|  _ \ / _ \ 
 | |_| | | | | |_) || |  / / | | |  \| | | | | || |  | |_) | |_) | | | |
 |  _  | |_| |  _ < | | / /| |_| | |\  | | |_| || |  |  __/|  _ <| |_| |
 |_| |_|\___/|_| \_\___/____\___/|_| \_|  \___/|___| |_|   |_| \_\\___/ 
                                                                                                                                                                                                                                                                                                                                       
=========================================================
* Horizon UI Dashboard PRO - v1.0.0
=========================================================

* Product Page: https://www.horizon-ui.com/pro/
* Copyright 2022 Horizon UI (https://www.horizon-ui.com/)

* Designed and Coded by Simmmple

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

import React, { useState, useEffect } from "react";

// Chakra imports
import {
  Flex,
  useColorModeValue,
  FormControl,
  Button,
  Text,
  FormLabel,
  Switch,
} from "@chakra-ui/react";
import Card from "components/card/Card.js";

// Custom components

export default function ProjectSettings({
  project_path,
  user,
  setProject,
  project,
}) {
  const textColorPrimary = useColorModeValue("secondaryGray.900", "white");
  const textColorSecondary = "secondaryGray.600";
  const [activeMode, setActiveMode] = useState(0);
  const [currentProject, setCurrentProject] = useState(null);
  const [gender, setGender] = useState(project?.data?.member_mandatory?.gender);
  const [dob, setDob] = useState(project?.data?.member_mandatory?.dob);
  const [social, setSocial] = useState(project?.data?.member_mandatory?.social);
  const [errorMsg, setErrorMsg] = useState("");
  const [successMsg, setSuccessMsg] = useState("");
  const [inProgress, setInProgress] = useState(false);

  const showMessage = () => {
    setSuccessMsg("Saved");
    setInterval(() => {
      setSuccessMsg("");
    }, 2000);
  };

  const handleSave = async () => {
    if (inProgress) return;
    if (!user) return;
    setErrorMsg("");
    setInProgress(true);

    const body = {
      project: {
        data: {
          member_mandatory: {
            gender: gender,
            dob: dob,
            social: social,
          },
        },
      },
    };
    console.log("UPDATE PROJECT SETTINGS: ", JSON.stringify(body));
    const response = await fetch(project_path, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: user,
      },
      body: JSON.stringify(body),
    });
    const json = await response.json();
    console.log("JSON: ", JSON.stringify(json));
    setInProgress(false);
    if (!response.ok) {
      setErrorMsg(json.error.message);
    }
    if (response.ok) {
      showMessage();
      console.log("RES: ", JSON.stringify(json));
    }
    return;
  };

  useEffect(() => {
    if (project) setCurrentProject(project);
  }, [project]);

  // Chakra Color Mode
  return (
    <FormControl
      w="60%"
      minW="300px"
      isDisabled={activeMode == 1 ? false : true}
    >
      <Card mb="20px">
        <Text
          fontSize="md"
          color={textColorPrimary}
          fontWeight="bold"
          marginInlineStart={"10px"}
          mb={"20px"}
          alignSelf={"center"}
        >
          Required Information:
        </Text>
        <Flex
          alignContent={"center"}
          textAlign="center"
          justifyContent={"center"}
        >
          <FormControl w="50%">
            <Flex justifyContent={"space-between"}>
              <FormLabel htmlFor="isChecked">Gender:</FormLabel>
              <Switch
                onChange={() => setGender(!gender)}
                id="gender"
                size="lg"
                isChecked={gender}
              />
            </Flex>
            <Flex justifyContent={"space-between"}>
              <FormLabel htmlFor="isChecked">Date of Birth:</FormLabel>
              <Switch
                onChange={() => setDob(!dob)}
                id="dob"
                size="lg"
                isChecked={dob}
              />
            </Flex>
            <Flex justifyContent={"space-between"}>
              <FormLabel htmlFor="isChecked">Social:</FormLabel>
              <Switch
                onChange={() => setSocial(!social)}
                id="social"
                size="lg"
                isChecked={social}
              />
            </Flex>
          </FormControl>
        </Flex>

        <Flex direction={"column"}>
          <Text color="red">{errorMsg}</Text>
          <Text color="green">{successMsg}</Text>
          <Flex direction={"row-reverse"} ms="10px" mt="40px">
            <Button
              onClick={() => setActiveMode(0)}
              variant={"outline"}
              minW={"100px"}
              size="md"
              fontWeight="500"
              borderRadius="70px"
            >
              Cancel
            </Button>
            <Button
              onClick={() => {
                if (!inProgress) handleSave();
              }}
              variant={"brand"}
              minW={"100px"}
              size="md"
              fontWeight="500"
              borderRadius="70px"
              mx="10px"
            >
              Save
            </Button>
          </Flex>
        </Flex>
      </Card>
    </FormControl>
  );
}
